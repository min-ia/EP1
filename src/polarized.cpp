#include "polarized.hpp"

Polarized::Polarized(string origin){
  setImage(origin);
  apply_filter();
  saveImage();
}

Polarized::Polarized(string origin, string name){
  setImage(origin);
  apply_filter();
  saveImage(name);
}

void Polarized::apply_filter(){

  int width = getWidth();
  int height = getHeight();
  int scale = getScale();
  char r, g, b;

  this->polarized_image = allocation_matrix();

  for(int i = 0; i < width; ++i){
    for(int j = 0; j < height; ++j){
      r = image[i][j].getPixelR();
      g = image[i][j].getPixelG();
      b = image[i][j].getPixelB();

      if((unsigned char)r < scale/2) polarized_image[i][j].setPixelR(0);
      else polarized_image[i][j].setPixelR(scale);

      if((unsigned char)g < scale/2) polarized_image[i][j].setPixelG(0);
      else polarized_image[i][j].setPixelG(scale);

      if((unsigned char)b < scale/2) polarized_image[i][j].setPixelB(0);
      else polarized_image[i][j].setPixelB(scale);
    }
  }
}

void Polarized::saveImage()
{
  string way = "doc/" + getName() + "_polarized.ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;

  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->polarized_image[i][j].getPixelR());
      new_image.put(this->polarized_image[i][j].getPixelG());
      new_image.put(this->polarized_image[i][j].getPixelB());
    }
  }
  new_image.close();
}

void Polarized::saveImage(string name)
{
  string way = "doc/" + name + ".ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;

  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->polarized_image[i][j].getPixelR());
      new_image.put(this->polarized_image[i][j].getPixelG());
      new_image.put(this->polarized_image[i][j].getPixelB());
    }
  }
  new_image.close();
}
