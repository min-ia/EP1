#include "gray_scale.hpp"

GrayScale::GrayScale(string origin){
  setImage(origin);
  apply_filter();
  saveImage();
}

GrayScale::GrayScale(string origin, string name){
  setImage(origin);
  apply_filter();
  saveImage(name);
}

void GrayScale::apply_filter(){

  int width = getWidth();
  int height = getHeight();
  char r, g, b;

  this->gray_scale_image = allocation_matrix();

  double grayscale_value;

  for(int i = 0; i < width; ++i){
    for(int j = 0; j < height; ++j){
      r = (unsigned char) image[i][j].getPixelR();
      g = (unsigned char) image[i][j].getPixelG();
      b = (unsigned char) image[i][j].getPixelB();

      grayscale_value = (0.299 * r) + (0.587 * g) + (0.144 * b);

      gray_scale_image[i][j].setPixelR((unsigned int)grayscale_value);
      gray_scale_image[i][j].setPixelG((unsigned int)grayscale_value);
      gray_scale_image[i][j].setPixelB((unsigned int)grayscale_value);
    }
  }
}

void GrayScale::saveImage()
{
  string way = "doc/" + getName() + "_gray.ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;

  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->gray_scale_image[i][j].getPixelR());
      new_image.put(this->gray_scale_image[i][j].getPixelG());
      new_image.put(this->gray_scale_image[i][j].getPixelB());
    }
  }
  new_image.close();
}

void GrayScale::saveImage(string name)
{
  string way = "doc/" + name + ".ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;

  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->gray_scale_image[i][j].getPixelR());
      new_image.put(this->gray_scale_image[i][j].getPixelG());
      new_image.put(this->gray_scale_image[i][j].getPixelB());
    }
  }
  new_image.close();
}
