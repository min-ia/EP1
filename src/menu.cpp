#include "menu.hpp"

Menu::Menu(){}

void Menu::setImage(string image)
{
  this->image = image;
}

string Menu::getImage()
{
  return this->image;
}

bool Menu::menu()
{
  system("clear");
  cout << "Filtros para Imagens"  << endl << endl;
  cout << "1. Selecionar imagem"  << endl;
  cout << "0. Sair"         << endl << endl;

  string image;
  char option;

  do{
    cout << "Selecionar: ";
    cin >> option;

    switch (option) {
      case '1':
        cout << endl << "Digite o nome da Imagem: ";
        cin >> image;
        if(verifyImageExist(image))
        {
          setImage(image);
          menu_filter();
          return true;
        } else{
          option = '2';
        }
        break;
      case '0':
        return false;
      default:
        cout << "Escolha uma opção valida." << endl;
        break;
    }
  }while(option != '0' && option != '1');

  return false;
}

void Menu::message()
{
  system("clear");
  cout << "Imagem: " << getImage() << ".ppm" << endl << endl;
  cout << "Filtro aplicado com sucesso." << endl;
  cout << "Aperte <enter> para continuar." << endl;
  char buffer;
  scanf("%*c%c", &buffer);
}

void Menu::menu_filter()
{
  Negative *negative;
  Polarized *polarized;
  GrayScale *gray;

  bool run = true;
  do{
    system("clear");
    cout << "Filtros para Imagens"  << endl << endl;
    cout << "Imagem: " << getImage() << ".ppm" << endl << endl;

    cout << "1. Filtro Negativo"        << endl;
    cout << "2. Filtro Polarizado"      << endl;
    cout << "3. Filtro Petro e Branco"  << endl;
    cout << "0. Voltar" << endl         << endl;
    cout << "Selecionar: ";

    bool flag = true;
    char option;
    do{
      cin >> option;

      flag = false;
      string name;
      switch(option) {
        case '1':
          name = readName();
          if(name == "NOT_NAME") negative = new Negative(image);
          else negative = new Negative(image, name);
          delete(negative);
          message();
          break;
        case '2':
          name = readName();
          if(name == "NOT_NAME") polarized = new Polarized(image);
          else polarized = new Polarized(image, name);
          delete(polarized);
          message();
          break;
        case '3':
          name = readName();
          if(name == "NOT_NAME") gray = new GrayScale(image);
          else gray = new GrayScale(image, name);
          delete(gray);
          message();
          break;
        case '0':
          run = false;
          break;
        default:
          cout << "Escolha uma opcao valida" << endl;
          flag = true;
          break;
      }
    }while(flag);

  }while(run);
}

string Menu::readName(){
  string name;
  char option;

    system("clear");
    cout << "Filtros para Imagens"  << endl << endl;
    cout << "Imagem: " << getImage() << ".ppm" << endl << endl;
    cout << "Deseja inserir um nome para a nova imagem? [s/n]" << endl;
    cout << "Caso opte por nao inserir, o nome sera gerado automaticamente." << endl;
  do{
    cout << "Selecionar: ";
    cin >> option;
    switch (option){
      case 's':
      case 'S':
        cout << "Nova imagem: ";
        cin >> name;
        return name;
      case 'n':
      case 'N':
        return "NOT_NAME";
      default:
        cout << "Escolha uma opção válida." << endl;
        break;
    }
}while(true);

return "NOT_NAME";
}

bool Menu::verifyImageExist(string origin){
  string way = "doc/" + origin + ".ppm";
  ifstream image_file(way.c_str());

  if(not image_file.is_open()){
    cout << "Arquivo nao identificado." << endl;
    return false;
  }
  image_file.close();
  return true;
}
