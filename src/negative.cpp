#include "negative.hpp"

Negative::Negative(string origin)
{
  setImage(origin);
  apply_filter();
  saveImage();
}

Negative::Negative(string origin, string name)
{
  setImage(origin);
  apply_filter();
  saveImage(name);
}

void Negative::apply_filter()
{
  int width = getWidth();
  int height = getHeight();
  int scale = getScale();
  char r, g, b;

  this->image_negative = allocation_matrix();

  for(int i = 0; i < width; ++i)
  {
    for(int j = 0; j < height; ++j)
    {
        r = image[i][j].getPixelR();
        g = image[i][j].getPixelG();
        b = image[i][j].getPixelB();

        image_negative[i][j].setPixelR(scale - r);
        image_negative[i][j].setPixelG(scale - g);
        image_negative[i][j].setPixelB(scale - b);
    }
  }
}

void Negative::saveImage()
{
  string way = "doc/" + getName() + "_negative.ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;

  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->image_negative[i][j].getPixelR());
      new_image.put(this->image_negative[i][j].getPixelG());
      new_image.put(this->image_negative[i][j].getPixelB());
    }
  }
  new_image.close();
}

void Negative::saveImage(string name)
{
  string way = "doc/" + name + ".ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;

  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->image_negative[i][j].getPixelR());
      new_image.put(this->image_negative[i][j].getPixelG());
      new_image.put(this->image_negative[i][j].getPixelB());
    }
  }
  new_image.close();
}
