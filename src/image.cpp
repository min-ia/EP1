#include "image.hpp"

Image::Image() {}

Image::Image(string origin) {
  setImage(origin);
}

void Image::setType(string type)
{
  this->type = type;
}

string Image::getType()
{
  return this->type;
}

void Image::Image::setName(string name)
{
  this->name = name;
}

string Image::Image::getName()
{
    return this->name;
}

void Image::setWidth(int width)
{
    this->image_width = width;
}

int Image::getWidth()
{
    return this->image_width;
}

void Image::setHeight(int height)
{
  this->image_height = height;
}

int Image::getHeight()
{
  return this->image_height;
}

void Image::setScale(int scale)
{
  this->scale = scale;
}

int Image::getScale()
{
    return this->scale;
}

void Image::setImage(string origin){

  setName(origin);
  string way = "doc/" + getName() + ".ppm";
  ifstream image_file(way.c_str());

  if(not image_file.is_open()){
    cout << "Falha na abertura do arquivo." << endl;
    exit(0);
  }

  string line;
  for(int i = 0; i < 4; i++){
    image_file >> line;

    if(line[0] == '#'){
      i--;
      getline(image_file, line);
    }
    else{
      switch(i){
        case 0: setType(line); break;
        case 1: setWidth(stoi(line.c_str())); break;
        case 2: setHeight(stoi(line.c_str())); break;
        case 3: setScale(stoi(line.c_str())); break;
      }
    }
  }
  char buffer;
  image_file.get(buffer);

  char r, g, b;
  int width = getWidth();
  int height = getHeight();
  this->image = allocation_matrix();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      image_file.get(r);
      image_file.get(g);
      image_file.get(b);
      image[i][j].setPixel(r, g, b);
    }
  }
  image_file.close();
}

void Image::saveImage(){

  string way = "doc/" + getName() + "_new.ppm";
  ofstream new_image(way.c_str());
  new_image << getType() << endl;
  new_image << getWidth() << " ";
  new_image << getHeight() << endl;
  new_image << getScale() << endl;
  int width = getWidth();
  int height = getHeight();

  for(int i = 0; i < width; i++)
  {
    for(int j = 0; j < height; j++)
    {
      new_image.put(this->image[i][j].getPixelR());
      new_image.put(this->image[i][j].getPixelG());
      new_image.put(this->image[i][j].getPixelB());
    }
  }
  new_image.close();
}

Pixel** Image::allocation_matrix()
{
  int width = getWidth();
  int height = getHeight();
  Pixel **matrix;
  matrix = new Pixel*[width];
  for(int i = 0; i < width; i++)
  {
    matrix[i] = new Pixel[height];
  }
  return matrix;
}
