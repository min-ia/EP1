# Orientação a Objetos 2/2016
* Aluna: Iasmin Santos Mendes
* Matricula: 14/0041940

## EP1 - C++
Exercício Programa 1 da disciplina de Orientação a Objetos da Universidade de Brasília. O programa consiste na aplicação dos filtros Negativo, Polarizado e Preto e Branco, sobre imagens de extensão .ppm.

### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Salve a imagem que será utilizada na pasta **doc**
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
* Digite 1 para selecionar uma imagem ou 0 para finalizar a aplicação.
* Caso opte por selecionar a imagem, deve-se em seguida inserir o nome da imagme, somente com o nome do arquivo **sem o caminho e sem a extensao .ppm**. É necessário que a imagem utilizada esteja na pasca **doc**.
	Exemplo: [certo] unb    [errado] doc/unb.ppm
* Entre com o valor referente ao filtro que deve ser aplicado de acordo com o menu exibido na tela.
* Caso deseje **nomear** a nova imagem, insira **'s'** no menu apresentado e digite o nome da nova imagem. Se selecionado **'n'** a imagem criada será **nomeada automaticamente**. Importante ressaltar que, se já existir na pasta uma imagem com o mesmo nome gerado pela aplicação, esta será sobrescrita pelo programa.
* Caso deseje, pode selecionar outros filtros na mesma imagem.
* Optando por **Voltar** no menu de filtros, pode-se selecionar uma outra imagem ou **sair** do programa.

