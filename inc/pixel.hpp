#ifndef PIXEL_HPP
#define PIXEL_HPP

class Pixel{

  private:
    int r;
    int g;
    int b;

  public:
    Pixel();
    Pixel(int r, int g, int b);
    void setPixel(int r, int g, int b);
    void setPixelR(int r);
    int getPixelR();
    void setPixelG(int g);
    int getPixelG();
    void setPixelB(int b);
    int getPixelB();
};

#endif
