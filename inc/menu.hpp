#ifndef MENU_HPP
#define MENU_HPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "pixel.hpp"
#include "image.hpp"
#include "negative.hpp"
#include "polarized.hpp"
#include "gray_scale.hpp"
#include "menu.hpp"

using namespace std;

class Menu
{
  private:
    string image;

  public:
    Menu();
    void setImage(string image);
    string getImage();
    bool menu();
    void message();
    void menu_filter();
    string readName();
    bool verifyImageExist(string origin);
};

#endif
