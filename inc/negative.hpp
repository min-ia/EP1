#ifndef NEGATIVE_HPP
#define NEGATIVE_HPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "pixel.hpp"
#include "image.hpp"

using namespace std;

class Negative: public Image {

  private:
    Pixel **image_negative;

  public:
    Negative(string origin);
    Negative(string origin, string name);
    void apply_filter();
    void saveImage();
    void saveImage(string name);
};

#endif
