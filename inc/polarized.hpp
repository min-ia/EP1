#ifndef POLARIZED_HPP
#define POLARIZED_HPP

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>

#include "pixel.hpp"
#include "image.hpp"

using namespace std;

class Polarized: public Image {

  private:
    Pixel** polarized_image;

  public:
    Polarized(string origin);
    Polarized(string origin, string name);
    void apply_filter();
    void saveImage();
    void saveImage(string name);
};

#endif
